using PX.Data;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN.Overrides.INDocumentRelease;
using PX.Objects.IN;
using PX.Objects;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.Text;
using System;

namespace PX.Objects.IN
{
      [PXNonInstantiatedExtension]
  public class IN_INKitRegister_ExistingColumn : PXCacheExtension<PX.Objects.IN.INKitRegister>
  {
      #region TranTranDesc  
      [PXDBString(60, IsUnicode = true,BqlField = typeof(INTran.tranDesc))]
      [PXUIField(DisplayName = "Asssembly Description", Visibility = PXUIVisibility.SelectorVisible)]
      [PXFormula(typeof(Selector<INKitRegister.kitInventoryID, InventoryItem.descr>))]
      public string TranTranDesc { get; set; }
      #endregion
  }
}