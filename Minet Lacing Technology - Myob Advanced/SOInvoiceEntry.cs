using System;
using System.Collections;
using System.Collections.Generic;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.TX;
using ItemLotSerial = PX.Objects.IN.Overrides.INDocumentRelease.ItemLotSerial;
using SiteLotSerial = PX.Objects.IN.Overrides.INDocumentRelease.SiteLotSerial;
using LocationStatus = PX.Objects.IN.Overrides.INDocumentRelease.LocationStatus;
using LotSerialStatus = PX.Objects.IN.Overrides.INDocumentRelease.LotSerialStatus;
using POLineType = PX.Objects.PO.POLineType;
using POReceiptLine = PX.Objects.PO.POReceiptLine;
using SiteStatus = PX.Objects.IN.Overrides.INDocumentRelease.SiteStatus;
using System.Linq;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects;
using PX.Objects.SO;

namespace PX.Objects.SO
{

    public class SOInvoiceEntry_Extension : PXGraphExtension<SOInvoiceEntry>
    {

        #region Event Handlers

        protected void ARInvoice_CustomerLocationID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e, PXFieldUpdated InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (ARInvoice)e.Row;

            if (row != null)
            {
                Location loc = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>, And<Location.bAccountID, Equal<Required<Location.bAccountID>>>>>
                               .Select(Base, row.CustomerLocationID, row.CustomerID);

                if (loc != null)
                {
                    LocationExt locext = PXCache<Location>.GetExtension<LocationExt>(loc);

                    if (locext != null)
                    {
                        ARInvoiceExt invoiceext = PXCache<ARInvoice>.GetExtension<ARInvoiceExt>(row);
                        invoiceext.UsrMAWACommodity = locext.UsrMAWACommodity;
                        invoiceext.UsrMAWAMarket = locext.UsrMAWAMarket;
                        invoiceext.UsrMAWARegion = locext.UsrMAWARegion;
                    }
                }
            }
        }

        protected void ARTran_RowInserting(PXCache cache, PXRowInsertingEventArgs e, PXRowInserting InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (ARTran)e.Row;

            ARInvoice invoice = this.Base.CurrentDocument.Current;
            ARInvoiceExt invoiceext = PXCache<ARInvoice>.GetExtension<ARInvoiceExt>(invoice);

            if (invoice != null)
            {
                ARTranExt artranext = PXCache<ARTran>.GetExtension<ARTranExt>(row);
                artranext.UsrMAWACommodity = invoiceext.UsrMAWACommodity;
                artranext.UsrMAWAMarket = invoiceext.UsrMAWAMarket;
                artranext.UsrMAWARegion = invoiceext.UsrMAWARegion;
            }
        }



        public delegate void InvoiceOrderDelegate(DateTime invoiceDate, PXResult<SOOrderShipment, SOOrder, CurrencyInfo, SOAddress, SOContact, SOOrderType> order, PXResultset<SOShipLine, SOLine> details, Customer customer, DocumentList<ARInvoice, SOInvoice> list);
        [PXOverride]
        public void InvoiceOrder(DateTime invoiceDate, PXResult<SOOrderShipment, SOOrder, CurrencyInfo, SOAddress, SOContact, SOOrderType> order, PXResultset<SOShipLine, SOLine> details, Customer customer, DocumentList<ARInvoice, SOInvoice> list, InvoiceOrderDelegate baseMethod)
        {
            baseMethod(invoiceDate, order, details, customer, list);

            if (list == null) this.Base.Save.Press();

            ARInvoice newdoc = this.Base.Document.Current;

            if (newdoc != null)
            {
                SOOrderExt soorderext = PXCache<SOOrder>.GetExtension<SOOrderExt>(order);

                SOInvoiceEntry graph = PXGraph.CreateInstance<SOInvoiceEntry>();
                graph.Document.Current = graph.Document.Search<ARInvoice.docType, ARInvoice.refNbr>(newdoc.DocType, newdoc.RefNbr);

                ARInvoice invoice = this.Base.Document.Current;
                ARInvoiceExt invoiceext = PXCache<ARInvoice>.GetExtension<ARInvoiceExt>(invoice);

                invoiceext.UsrMAWAMarket = soorderext.UsrMAWAMarket;
                invoiceext.UsrMAWACommodity = soorderext.UsrMAWACommodity;
                invoiceext.UsrMAWARegion = soorderext.UsrMAWARegion;

                graph.Document.Update(invoice);
                //foreach (ARTran il in PXSelect<ARTran, Where<ARTran.shipmentType, Equal<Current<SOShipLine.shipmentType>>, And<SOShipLine.shipmentNbr, Equal<Current<SOShipLine.shipmentNbr>>>>>.Select(this.Base))
                foreach (ARTran line in this.Base.Transactions.Select())
                {
                    graph.Transactions.Current = graph.Transactions.Search<ARTran.tranType, ARTran.refNbr, ARTran.lineNbr>(line.TranType, line.RefNbr, line.LineNbr);
                    ARTran invline = graph.Transactions.Current;

                    SOLine soline = PXSelect<SOLine, Where<SOLine.orderType, Equal<Required<SOLine.orderType>>, And<SOLine.orderNbr, Equal<Required<SOLine.orderNbr>>, And<SOLine.lineNbr, Equal<Required<SOLine.lineNbr>>>>>>
                                    .Select(this.Base, invline.SOOrderType, invline.SOOrderNbr, invline.SOOrderLineNbr);


                    SOLineExt solineext = PXCache<SOLine>.GetExtension<SOLineExt>(soline);
                    ARTranExt invlineext = PXCache<ARTran>.GetExtension<ARTranExt>(invline);

                    invlineext.UsrMAWAMarket = solineext.UsrMAWAMarket;
                    invlineext.UsrMAWACommodity = solineext.UsrMAWACommodity;
                    invlineext.UsrMAWARegion = solineext.UsrMAWARegion;
                    graph.Transactions.Update(invline);
                }
                graph.Save.Press();



                if (list == null)
                {
                    this.Base.Document.Current = graph.Document.Current;
                    throw new PXRedirectRequiredException(graph, "Reloading Screen");
                }
            }
        }
        #endregion

    }


}