using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using PX.Common;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.DR;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PM;
using PX.Objects.PO;
using PX.Objects.TX;
using POLine = PX.Objects.PO.POLine;
using POOrder = PX.Objects.PO.POOrder;
using System.Threading.Tasks;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects;
using PX.Objects.SO;

namespace PX.Objects.SO
{

    public class SOOrderEntry_Extension : PXGraphExtension<SOOrderEntry>
    {

        #region Event Handlers

        protected void SOOrder_RowSelected(PXCache cache, PXRowSelectedEventArgs e)
        {

            var row = (SOOrder)e.Row;
            bool IsQuote = row.OrigOrderType == "QT";

            PXUIFieldAttribute.SetVisible<SOOrder.origOrderType>(cache, null, IsQuote);
            PXUIFieldAttribute.SetVisible<SOOrder.origOrderNbr>(cache, null, IsQuote);
        }

        protected void SOLine_RowInserting(PXCache cache, PXRowInsertingEventArgs e, PXRowInserting InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (SOLine)e.Row;

            SOOrder order = this.Base.CurrentDocument.Current;
            SOOrderExt soorderext = PXCache<SOOrder>.GetExtension<SOOrderExt>(order);

            if (order != null)
            {
                SOLineExt solineext = PXCache<SOLine>.GetExtension<SOLineExt>(row);
                solineext.UsrMAWACommodity = soorderext.UsrMAWACommodity;
                solineext.UsrMAWAMarket = soorderext.UsrMAWAMarket;
                solineext.UsrMAWARegion = soorderext.UsrMAWARegion;
            }
        }



        protected void SOOrder_CustomerLocationID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e, PXFieldUpdated InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (SOOrder)e.Row;

            if (row != null)
            {
                Location loc = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>, And<Location.bAccountID, Equal<Required<Location.bAccountID>>>>>
                               .Select(Base, row.CustomerLocationID, row.CustomerID);

                if (loc != null)
                {
                    LocationExt locext = PXCache<Location>.GetExtension<LocationExt>(loc);

                    if (locext != null)
                    {
                        SOOrderExt soorderext = PXCache<SOOrder>.GetExtension<SOOrderExt>(row);
                        soorderext.UsrMAWACommodity = locext.UsrMAWACommodity;
                        soorderext.UsrMAWAMarket = locext.UsrMAWAMarket;
                        soorderext.UsrMAWARegion = locext.UsrMAWARegion;
                    }
                }
            }
        }

        #endregion

    }
}