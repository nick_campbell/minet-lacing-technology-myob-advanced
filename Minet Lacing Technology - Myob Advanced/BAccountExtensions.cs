using PX.Data.EP;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.AR;
using PX.Objects.CR.MassProcess;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.TX;
using PX.Objects;
using PX.SM;
using PX.TM;
using System.Collections.Generic;
using System.Diagnostics;
using System;

namespace PX.Objects.CR
{
  public class BAccountExt : PXCacheExtension<PX.Objects.CR.BAccount>
  {
      #region UsrMAWACommodity
      [PXDBString(10)]
      [PXStringList(new string[] { "MiningHvy","MiningLte","MiningCoal","Agri","Aqua","Pharma","Farming","Food","IndustLte","IndustHvy"
},
        new string[] { "Mining Heavy","Mining Light","Mining Coal","Agriculture","Aquaculture","Pharmaceautical","Farming","Food","Industrial Light","Industrial Heavy"
})]
      [PXUIField(DisplayName="Commodity")]
      [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
      public virtual string UsrMAWACommodity { get; set; }
      public abstract class usrMAWACommodity : IBqlField { }
      #endregion

      #region UsrMAWAMarket
      
[PXDBString(10)]
      [PXStringList(new string[] { "MD1","D01", "D02", "EU1", "EU2", "R01","R02", "EX1", "EX2", "TBA"},
        new string[] { "Master Distributor","Distributor 01", "Distributor 02","End User 1", "End User 2", "Reseller 1","Reseller 2","Export 1", "Export 2", "To Be Advised" })]
      [PXUIField(DisplayName="Market")]
      [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
      public virtual string UsrMAWAMarket { get; set; }
      public abstract class usrMAWAMarket : IBqlField { }
      #endregion

      #region UsrMAWARegion
      [PXDBString(10)]
      [PXStringList(new string[]{"NSW01","NSW02","NSW03","NSW04","NSW05","NSW06","NSW07","NSW08","NT01","NT02","NT03","NT04","NT05","NT06","QLD01","QLD02","QLD03","QLD04","QLD05","QLD06","QLD07","SA01","SA02","SA03","SA04","SA05","SA06","SA07","SA08","VIC01","VIC02","VIC03","VIC04","VIC05","VIC06","VIC07","VIC08","VIC09","WA01","WA02","WA03","WA04","WA05","WA06","WA07","WA08","WA09","WA10","NZL","INT"},
        new string[] { "NSW01-Sydney","NSW02-Blue Mountains","NSW03-Heart of Country","NSW04-North Coast","NSW05-Outback","NSW06-Snowy Mountains","NSW07-South Coast","NSW08-ACT","NT01-Darwin","NT02-Top End Rural","NT03-East Arnhem","NT04-Katherine","NT05-Barkly","NT06-Central Australia","QLD01-South East","QLD02-Metropolitan Brisbane","QLD03-North Coast","QLD04-Darling Downs South West","QLD05-Central QLD","QLD06-North QLD","QLD07-Far North QLD","SA01-Metropolitan Adelaide","SA02-Outer Adelaide","SA04-Yorke Peninsula & Lower North","SA05-Murray Lands","SA06-The South East","SA07-Eyre Peninsula","SA08-Northern","The Outback","VIC01-Central","VIC02-North Central","VIC03-West & South Gippsland","VIC04-East Gippsland","VIC05-North East","VIC06-Nortjern Country","VIC07-Mallee","VIC08-Wimmera","VIC09-South West","WA01-Perth","WA02-Peel","WA03-South West","WA04-Greater Southern","WA05-Wheatbelt","WA06-Gascoyne","WA07-Kimberley","WA08-Pilbara","WA09-Mid West","WA10-Goldfields","NZL-New Zeland","INT-International Other"})]
      [PXUIField(DisplayName="Territory")]
      [PXDefault(PersistingCheck = PXPersistingCheck.Nothing)]
      public virtual string UsrMAWARegion { get; set; }
      public abstract class usrMAWARegion : IBqlField { }
      #endregion
  }
}