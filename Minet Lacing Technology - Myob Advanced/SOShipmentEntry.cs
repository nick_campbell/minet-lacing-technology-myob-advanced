using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using PX.Data;
using PX.Common;
using PX.Objects.AR;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.EP;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.SM;
using PX.TM;
using SiteStatus = PX.Objects.IN.Overrides.INDocumentRelease.SiteStatus;
using LocationStatus = PX.Objects.IN.Overrides.INDocumentRelease.LocationStatus;
using LotSerialStatus = PX.Objects.IN.Overrides.INDocumentRelease.LotSerialStatus;
using ItemLotSerial = PX.Objects.IN.Overrides.INDocumentRelease.ItemLotSerial;
using SiteLotSerial = PX.Objects.IN.Overrides.INDocumentRelease.SiteLotSerial;
using POLineType = PX.Objects.PO.POLineType;
using POReceipt = PX.Objects.PO.POReceipt;
using POReceiptLine = PX.Objects.PO.POReceiptLine;
using System.Diagnostics;
using PX.Objects.PO;
using PX.Objects;
using PX.Objects.SO;

namespace PX.Objects.SO
{

    public class SOShipmentEntry_Extension : PXGraphExtension<SOShipmentEntry>
    {

        #region Event Handlers 
        public delegate void CreateShipmentDelegate(SOOrder order, Nullable<Int32> SiteID, Nullable<DateTime> ShipDate, Nullable<Boolean> useOptimalShipDate, String operation, DocumentList<SOShipment> list);
        [PXOverride]
        public void CreateShipment(SOOrder order, Nullable<Int32> SiteID, Nullable<DateTime> ShipDate, Nullable<Boolean> useOptimalShipDate, String operation, DocumentList<SOShipment> list, CreateShipmentDelegate baseMethod)
        {
            baseMethod(order, SiteID, ShipDate, useOptimalShipDate, operation, list);

            if (list == null) this.Base.Save.Press();

            SOShipment newdoc = this.Base.Document.Current;

            if (newdoc != null)
            {
                SOOrderExt soorderext = PXCache<SOOrder>.GetExtension<SOOrderExt>(order);

                SOShipmentEntry graph = PXGraph.CreateInstance<SOShipmentEntry>();
                graph.Document.Current = graph.Document.Search<SOShipment.shipmentNbr>(newdoc.ShipmentNbr);

                SOShipment shipment = graph.Document.Current;
                SOShipmentExt shipmentext = PXCache<SOShipment>.GetExtension<SOShipmentExt>(shipment);

                shipmentext.UsrMAWAMarket = soorderext.UsrMAWAMarket;
                shipmentext.UsrMAWACommodity = soorderext.UsrMAWACommodity;
                shipmentext.UsrMAWARegion = soorderext.UsrMAWARegion;

                graph.Document.Update(shipment);

                foreach (SOShipLine sl in PXSelect<SOShipLine, Where<SOShipLine.shipmentType, Equal<Current<SOShipLine.shipmentType>>, And<SOShipLine.shipmentNbr, Equal<Current<SOShipLine.shipmentNbr>>>>>.Select(this.Base))
                {

                    graph.Transactions.Current = graph.Transactions.Search<SOShipLine.shipmentNbr, SOShipLine.lineNbr>(sl.ShipmentNbr, sl.LineNbr);
                    SOShipLine shipline = graph.Transactions.Current;

                    SOLine soline = PXSelect<SOLine, Where<SOLine.orderType, Equal<Required<SOLine.orderType>>, And<SOLine.orderNbr, Equal<Required<SOLine.orderNbr>>, And<SOLine.lineNbr, Equal<Required<SOLine.lineNbr>>>>>>
                                    .Select(this.Base, shipline.OrigOrderType, shipline.OrigOrderNbr, shipline.OrigLineNbr);
                    SOLineExt solineext = PXCache<SOLine>.GetExtension<SOLineExt>(soline);

                    SOShipLineExt shiplineext = PXCache<SOShipLine>.GetExtension<SOShipLineExt>(shipline);

                    shiplineext.UsrMAWAMarket = solineext.UsrMAWAMarket;
                    shiplineext.UsrMAWACommodity = solineext.UsrMAWACommodity;
                    shiplineext.UsrMAWARegion = solineext.UsrMAWARegion;

                    graph.Transactions.Update(shipline);
                }
                graph.Save.Press();

                this.Base.Document.Current = graph.Document.Current;
                if (list == null) throw new PXRedirectRequiredException(graph, "Reloading Screen");
            }
        }

        #endregion

    }


}