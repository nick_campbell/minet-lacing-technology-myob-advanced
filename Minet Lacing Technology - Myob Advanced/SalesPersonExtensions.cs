using PX.Data.EP;
using PX.Data;
using PX.Objects.AR;
using PX.Objects.GL;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.AR
{
  public class SalesPersonExt : PXCacheExtension<PX.Objects.AR.SalesPerson>
  {
      #region UsrWSMAEmail
      [PXDBString(200)]
      [PXUIField(DisplayName="Email")]

      public virtual string UsrWSMAEmail { get; set; }
      public abstract class usrWSMAEmail : IBqlField { }
      #endregion
  }
}