using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using PX.Common;
using PX.Objects.Common;
using PX.Data;
using PX.Objects.GL;
using PX.Objects.CM;
using PX.Objects.CS;
using PX.Objects.CR;
using PX.Objects.TX;
using PX.Objects.IN;
using PX.Objects.BQLConstants;
using PX.Objects.EP;
using PX.Objects.SO;
using PX.Objects.DR;
using SOInvoice = PX.Objects.SO.SOInvoice;
using SOInvoiceEntry = PX.Objects.SO.SOInvoiceEntry;
using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Objects.GL.Reclassification.UI;
using PX.Objects;
using PX.Objects.AR;

namespace PX.Objects.AR
{

    public class ARInvoiceEntry_Extension : PXGraphExtension<ARInvoiceEntry>
    {

        #region Event Handlers

        protected void ARInvoice_CustomerLocationID_FieldUpdated(PXCache cache, PXFieldUpdatedEventArgs e, PXFieldUpdated InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (ARInvoice)e.Row;

            if (row != null)
            {
                Location loc = PXSelect<Location, Where<Location.locationID, Equal<Required<Location.locationID>>, And<Location.bAccountID, Equal<Required<Location.bAccountID>>>>>
                               .Select(Base, row.CustomerLocationID, row.CustomerID);

                if (loc != null)
                {
                    LocationExt locext = PXCache<Location>.GetExtension<LocationExt>(loc);

                    if (locext != null)
                    {
                        ARInvoiceExt invoiceext = PXCache<ARInvoice>.GetExtension<ARInvoiceExt>(row);
                        invoiceext.UsrMAWACommodity = locext.UsrMAWACommodity;
                        invoiceext.UsrMAWAMarket = locext.UsrMAWAMarket;
                        invoiceext.UsrMAWARegion = locext.UsrMAWARegion;
                    }
                }
            }
        }

        protected void ARTran_RowInserting(PXCache cache, PXRowInsertingEventArgs e, PXRowInserting InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null)
                InvokeBaseHandler(cache, e);
            var row = (ARTran)e.Row;

            ARInvoice invoice = this.Base.CurrentDocument.Current;
            ARInvoiceExt invoiceext = PXCache<ARInvoice>.GetExtension<ARInvoiceExt>(invoice);

            if (invoice != null)
            {
                ARTranExt artranext = PXCache<ARTran>.GetExtension<ARTranExt>(row);
                artranext.UsrMAWACommodity = invoiceext.UsrMAWACommodity;
                artranext.UsrMAWAMarket = invoiceext.UsrMAWAMarket;
                artranext.UsrMAWARegion = invoiceext.UsrMAWARegion;
            }
        }

        #endregion
    }
}