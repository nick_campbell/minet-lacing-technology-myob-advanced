using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PX.Common;
using PX.Data;
using PX.Objects.CA;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.SO;
using PX.SM;
using PX.Objects.AR.CCPaymentProcessing;
using PX.Objects;
using PX.Objects.AR;

namespace PX.Objects.AR
{

    public class CustomerMaint_Extension : PXGraphExtension<CustomerMaint>
    {

        #region Event Handlers


        protected void Customer_RowPersisted(PXCache cache, PXRowPersistedEventArgs e, PXRowPersisted InvokeBaseHandler)
        {
            if (InvokeBaseHandler != null) InvokeBaseHandler(cache, e);
            var row = (Customer)e.Row;

            if (cache.GetStatus(e.Row) == PXEntryStatus.Deleted) return;

            if (row != null && e.TranStatus == PXTranStatus.Completed)  // run on second persisted event
            {
                BAccount baccount = this.Base.BAccount.Current;
                BAccountExt baccountext = PXCache<BAccount>.GetExtension<BAccountExt>(baccount);

                LocationExtAddress locextadd = this.Base.DefLocation.Select();

                if (locextadd != null)
                {
                    LocationMaint graph = PXGraph.CreateInstance<CustomerLocationMaint>();
                    graph.Location.Current = graph.Location.Search<Location.locationID>(locextadd.LocationID, locextadd.BAccountID);
                    Location loc = graph.Location.Cache.CreateCopy(graph.Location.Current) as Location;


                    graph.ARAccountSubLocation.Current = PXSelect<LocationARAccountSub, Where<LocationARAccountSub.bAccountID, Equal<Required<Location.bAccountID>>, And<LocationARAccountSub.locationID, Equal<Required<Location.cARAccountLocationID>>>>>
                                                .Select(Base, loc.BAccountID, loc.CARAccountLocationID);

                    LocationExt locext = PXCache<Location>.GetExtension<LocationExt>(loc);

                    locext.UsrMAWAMarket = baccountext.UsrMAWAMarket;
                    locext.UsrMAWACommodity = baccountext.UsrMAWACommodity;
                    locext.UsrMAWARegion = baccountext.UsrMAWARegion;

                    LocationARAccountSub lars = graph.ARAccountSubLocation.Cache.CreateCopy(graph.ARAccountSubLocation.Current) as LocationARAccountSub;

                    if (loc.IsARAccountSameAsMain == false)
                    {
                        loc.CARAccountID = lars.CARAccountID;
                        loc.CARSubID = lars.CARSubID;
                        loc.CARAccountLocationID = lars.LocationID;
                    }
                    else
                    {
                        loc.CARAccountID = null;
                        loc.CARSubID = null;
                    }

                    graph.Location.Update(loc);
                    LocationARAccountSub copyloc = new LocationARAccountSub();
                    copyloc.BAccountID = lars.BAccountID;
                    copyloc.LocationID = lars.LocationID;
                    copyloc.CARAccountID = lars.CARAccountID;
                    copyloc.CARSubID = lars.CARSubID;
                    graph.ARAccountSubLocation.Update(copyloc);

                    graph.Location.Cache.SetStatus(loc, PXEntryStatus.Updated);
                    graph.ARAccountSubLocation.Cache.SetStatus(lars, PXEntryStatus.Updated);
                    
                    graph.Persist();
                    //graph.Save.Press();
                }
            }
        }





        #endregion

    }


}