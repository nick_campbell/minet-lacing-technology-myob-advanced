using CRLocation = PX.Objects.CR.Standalone.Location;
using PX.Data.ReferentialIntegrity.Attributes;
using PX.Data;
using PX.Objects.AP;
using PX.Objects.CM;
using PX.Objects.CR;
using PX.Objects.CS;
using PX.Objects.GL;
using PX.Objects.IN;
using PX.Objects.PO;
using PX.Objects;
using System.Collections.Generic;
using System;

namespace PX.Objects.PO
{
  public class POReceiptExt : PXCacheExtension<PX.Objects.PO.POReceipt>
  {
      #region UsrMAWSETDDate
      [PXDBDate]
      [PXUIField(DisplayName="Estimated Departure Date")]

      public virtual DateTime? UsrMAWSETDDate { get; set; }
      public abstract class usrMAWSETDDate : IBqlField { }
      #endregion

      #region UsrMAWSETADate
      [PXDBDate]
      [PXUIField(DisplayName="Expected Arrival Date")]

      public virtual DateTime? UsrMAWSETADate { get; set; }
      public abstract class usrMAWSETADate : IBqlField { }
      #endregion

      #region UsrMAWSShippingLine
      [PXDBString(50)]
      [PXUIField(DisplayName="Shipping Line")]

      public virtual string UsrMAWSShippingLine { get; set; }
      public abstract class usrMAWSShippingLine : IBqlField { }
      #endregion

      #region UsrMAWSVessel
      [PXDBString(50)]
      [PXUIField(DisplayName="Vessel")]

      public virtual string UsrMAWSVessel { get; set; }
      public abstract class usrMAWSVessel : IBqlField { }
      #endregion

      #region UsrMAWSContainer
      [PXDBString(50)]
      [PXUIField(DisplayName="Container")]

      public virtual string UsrMAWSContainer { get; set; }
      public abstract class usrMAWSContainer : IBqlField { }
      #endregion

      #region UsrMAWSLFFA
      [PXDBString(50)]
      [PXUIField(DisplayName="Local Freight Forwarder Agent")]

      public virtual string UsrMAWSLFFA { get; set; }
      public abstract class usrMAWSLFFA : IBqlField { }
      #endregion
  }
}